package kg.numbertoword;

public class NumberSpelling {
	
public static int[] digitCalculator(int userNumber, int numberOfDigits) {
		
		int counter = 0;
		
		int[] storedDigits = new int[numberOfDigits];
		
		int temp = 0;
		
		while(userNumber > 0) {
			
			temp = userNumber % 10;
			
			storedDigits[counter] = temp;
			
			userNumber = userNumber / 10;
			
			counter++;
			
		}
		
		return storedDigits;
	}
	
	public static void printSingleDigits(String[] singleDigits, int[] reArray, int currentDigit) {
		
		System.out.print(singleDigits[reArray[currentDigit] - 1]);
	}
	
	public static void printDoubleDigits(int[] reArray, String[] singleDigits, String[] teenNumbers,
			String[] numbersByTen, int currentDigit) {
		
		// -- User input 10 - 19 --
		if(reArray[currentDigit] == 1) {
			System.out.print(teenNumbers[reArray[currentDigit - 1]]);
		}
		
		// -- User input 20 - 99 --
		if(reArray[currentDigit] > 1) {
			if(reArray[currentDigit - 1] == 0) {
				System.out.print(numbersByTen[reArray[currentDigit] - 2] + "");
			}else {
				System.out.print(numbersByTen[reArray[currentDigit] - 2] + " ");
				currentDigit--;
				printSingleDigits(singleDigits, reArray, currentDigit);
			}
		}
		
	}
	
	public static void printTripleDigits(int[] reArray, String[] singleDigits, String[] teenNumbers,
			String[] numbersByTen, String[] numbersByHundredsThousands, int currentDigit) {
		
		// -- User input (1 - 9)00 --
		
		if(reArray[currentDigit - 1] == 0 && reArray[currentDigit - 2] == 0) {
			
			printSingleDigits(singleDigits, reArray, currentDigit);
			System.out.print(" " + numbersByHundredsThousands[0]);
			
		}
		
		// -- User input (1 - 9)01 - (1 - 9)09 -- 
		
		else if(reArray[currentDigit - 1] == 0) {
			printSingleDigits(singleDigits, reArray, currentDigit);
			currentDigit--;
			System.out.print(" " + numbersByHundredsThousands[0] +" ");
			currentDigit--;
			printSingleDigits(singleDigits, reArray, currentDigit);
		}
		
		// -- User input (1 - 9)10 - (1 - 9)19  --
		
		else if(reArray[currentDigit - 1] == 1) {
			printSingleDigits(singleDigits, reArray, currentDigit);
			currentDigit--;
			System.out.print(" " + numbersByHundredsThousands[0] +" ");
			printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
		}
		
		// -- User input (2 - 9)(2 - 9)0
		
		else if(reArray[currentDigit - 1] > 1 && reArray[currentDigit - 2] == 0) {
			printSingleDigits(singleDigits, reArray, currentDigit);
			currentDigit--;
			System.out.print(" " + numbersByHundredsThousands[0] +" ");
			printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
		}
		
		// -- User input (2 - 9)21 - (2 - 9)99
		
		else /*(reArray[currentDigit - 1] > 1 && reArray[currentDigit - 2] != 0)*/ {
			printSingleDigits(singleDigits, reArray, currentDigit);
			currentDigit--;
			System.out.print(" " + numbersByHundredsThousands[0] +" " );
			printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
			
		}
		
	}
	
	public static void printNumbersAsWords(int userNumber) {
		
		if(userNumber < 1000000) {
			
			String[] singleDigits = {"One","Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
			
			String[] teenNumbers = {"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "FifTeen", "Sixteen",
					
					"Seventeen", "Eighteen", "Ninteen"};
			
			String[] numbersByTen = {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
			
			String[] numbersByHundredsThousands = {"Hundred", "Thousand"};
			
			String userNumberString = Integer.toString(userNumber);
			
			int numberOfDigits = userNumberString.length();
			
			int[] reArray = digitCalculator(userNumber, numberOfDigits);
			
			int currentDigit = numberOfDigits - 1;
			
			// -- User input 0 --
			
			if(userNumber == 0) {
				System.out.println("Zero");
				System.exit(0);
			}
			
			// -- User input 1 - 9 --
			if(numberOfDigits == 1) {
				printSingleDigits(singleDigits, reArray, currentDigit);
			}
			
			// -- User input 10 - 99 --
			
			if(numberOfDigits == 2) {
				
				printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
;
			}
			
			// -- User input 100 - 999 --
			
			if(numberOfDigits == 3) {
				
				printTripleDigits(reArray, singleDigits, teenNumbers, numbersByTen, numbersByHundredsThousands, currentDigit);
				
			}
			
			// -- User input 1000 - 9999 --
			
			if(numberOfDigits == 4) {
				printSingleDigits(singleDigits, reArray, currentDigit);
				System.out.print(" "+numbersByHundredsThousands[1] + " ");
				currentDigit--;
				if(reArray[currentDigit] == 0 && reArray[currentDigit - 1] == 0 && reArray[currentDigit - 2] == 0) {
					//System.exit(0);
				}else if(reArray[currentDigit] == 0 && reArray[currentDigit - 1] == 0) {
					currentDigit = currentDigit - 2;
					printSingleDigits(singleDigits, reArray, currentDigit);
				}else if((reArray[currentDigit]) == 0) {
					currentDigit--;
					printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
				}else if(reArray[currentDigit] != 0) {
					printTripleDigits(reArray, singleDigits, teenNumbers, numbersByTen, numbersByHundredsThousands, currentDigit);
				}
			}
			
			// -- User input 10000 - 99999 --
			
			if(numberOfDigits == 5) {
				
				printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
				System.out.print(" "+numbersByHundredsThousands[1]+" ");
				currentDigit = currentDigit - 2;
				if(reArray[currentDigit] == 0 && reArray[currentDigit - 1] == 0 && reArray[currentDigit - 2] == 0) {
					System.exit(0);
				}else if(reArray[currentDigit] == 0 && reArray[currentDigit - 1] == 0) {
					currentDigit = currentDigit - 2;
					printSingleDigits(singleDigits, reArray, currentDigit);
				}else if((reArray[currentDigit]) == 0) {
					currentDigit--;
					printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
				}else if(reArray[currentDigit] != 0) {
					printTripleDigits(reArray, singleDigits, teenNumbers, numbersByTen, numbersByHundredsThousands, currentDigit);
				}
			}
			
			// -- User input 100000 - 999999 --
			
			if(numberOfDigits == 6) {
				printTripleDigits(reArray, singleDigits, teenNumbers, numbersByTen, numbersByHundredsThousands, currentDigit);
				System.out.print(" "+numbersByHundredsThousands[1]+" ");
				currentDigit = currentDigit - 3;
				if(reArray[currentDigit] == 0 && reArray[currentDigit - 1] == 0 && reArray[currentDigit - 2] == 0) {
					System.exit(0);
				}else if(reArray[currentDigit] == 0 && reArray[currentDigit - 1] == 0) {
					currentDigit = currentDigit - 2;
					printSingleDigits(singleDigits, reArray, currentDigit);
				}else if((reArray[currentDigit]) == 0) {
					currentDigit--;
					printDoubleDigits(reArray, singleDigits, teenNumbers, numbersByTen, currentDigit);
				}else if(reArray[currentDigit] != 0) {
					printTripleDigits(reArray, singleDigits, teenNumbers, numbersByTen, numbersByHundredsThousands, currentDigit);
				}
				
				
			}
			
		}else {
			System.out.println("This program only prints out numbers until 1000000!");
			System.exit(0);
		}
		
	}

}
